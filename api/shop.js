import axios from 'axios'
import { host } from './constatns.js'

export const getShopList = () => axios.get(`${host}shop/list`)

export const getLoyaltiesById = (id) => axios.get(`${host}shop/${id}/loyalty`)

export const setLoyaltiesById = (id, data) => axios.post(`${host}shop/${id}/loyalty`, { ...data })

export const suggestOffersById = (id) => axios.get(`${host}shop/${id}/suggest/offers`)

export const getListPositionsById = (id) => axios.get(`${host}positions/${id}`)

export const createCartById = (id) => axios.post(`${host}create/cart/${id}`)

export const addToCart = (cartId, data) => axios.post(`${host}cart/${cartId}/add`, { ...data })

export const removeFromCart = (cartId, data) => axios.post(`${host}cart/${cartId}/remove`, {...data})