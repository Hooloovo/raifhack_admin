import axios from 'axios';
import { host } from './constatns'

export const setTestingDataById = (id, data) => axios.post(`${host}testing/${id}/add`, { ...data })

export const getTestingListById = (id) => axios.get(`${host}testing/${id}/list`)

export const disableTestingById = (id) => axios.post(`${host}testing/disable/${id}`)