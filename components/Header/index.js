import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import {useRouter} from "next/router";
import Link from 'next/link';

import {Container, Logo, Navbar, NavItem, SettingsButton, Subheader, SubheaderTitle} from './styled';
import option from './img/options.svg';

const getTitle = (path) => {
  if (path === '/analytics') {
    return 'Аналитика'
  } else if (path === '/personal') {
    return 'Персональные предложения'
  } else if (path === '/testing') {
    return 'Тестирование акций'
  } else {
    return 'Настройка системы лояльности'
  }
}

export const Header = () => {
  const router = useRouter();
  const [subheaderTitle, setSubheaderTitle] = useState(getTitle(router.pathname))
  if (router.pathname === '/analytics') {

  }

  useEffect(() => {
    setSubheaderTitle(getTitle(router.pathname))
  }, [router])

  return (
    <>
      <Container>
        <Logo src={'https://www.raiffeisen.ru/common/new/images/logo-raif.svg'}/>
        <Navbar>
          <Link href={'/analytics'}>
            <NavItem active={router.pathname === '/analytics'}>Аналитика</NavItem>
          </Link>
          <Link href={'/personal'}>
            <NavItem active={router.pathname === '/personal'}>Персональные предложения</NavItem>
          </Link>
          <Link href={'/testing'}>
            <NavItem active={router.pathname === '/testing'}>Тестирование акций</NavItem>
          </Link>
        </Navbar>
        <Link href={'/options'}>
        <SettingsButton>
          <img src={option}/>
          Система лояльности
        </SettingsButton>
        </Link>
      </Container>
      <Subheader>
          <SubheaderTitle>{subheaderTitle}</SubheaderTitle>
      </Subheader>
    </>
  );
}

export default Header;
