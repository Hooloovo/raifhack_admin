import styled from 'styled-components';
import Link from 'next/link';

export const Container = styled.header`
    width: 100%;
    height: 80px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 16px 48px;
`

export const Logo = styled.img`
    width: 167px;
    height: 37px;
`

export const Navbar = styled.div`
    width: 600px;
    
    display: flex;
    justify-content: space-between;
`

export const NavItem = styled.a`
    font-style: normal;
    font-weight: ${({ active }) => active ? 'bold' : 'normal'};
    font-size: 16px;
    line-height: 20px;
    color: #333333;
    cursor: pointer;
`

export const SettingsButton = styled.button`
    width: 340px;
    height: 48px;
    background-color: #333;
    display: flex;
    align-items: center;
    justify-content: center;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 16px;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    color: #FDEB4F;
    cursor: pointer;
    
    > img {
      margin-right: 8px;
    }
`

export const Subheader = styled.div`
    width: 100%;
    height: 80px;
    background-color: #333;
    padding: 16px 48px;
    margin-bottom: 40px;
`

export const SubheaderTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 43px;
    color: #FFFFFF;
`
