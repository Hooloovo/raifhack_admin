import React, { useState, useEffect } from 'react'
import { v4 } from 'uuid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { CategoryWithButtonsContainer, CategoryContainer, TextInput,
    PercentInput, InputStaticText, RedactButton,
    RemoveButton, 
    CategorySettings,
    SubText,
    SelectStyled,
    SaveButton} from './styled';
import edit from './img/edit.svg';
import remove from './img/remove.svg';

export const OptionsInput = ({
    name = 'Basic',
    percent = 5,
    type='total',
    limit='1000',
    onSave,
    id,
    onRemove,
}) => {
    const [nameValue, setName] = useState(name)
    const [percentValue, setPercent] = useState(percent)
    const [typeValue, setType] = useState(type)
    const [limitValue, setLimit] = useState(limit)
    const [isEdit, setEdit] = useState(false)
    const [val, setVal] = useState()
    const key = v4();

    const handleSave = () => {
        setEdit(false)
        onSave(id, {
            categoryName: nameValue,
            categoryType: typeValue === 'total' ? 'OrderSum' : 'CheckSum',
            categoryPercent: percentValue,
            categorySum: limitValue,
        })
    }

    return (
        <>
            <CategoryWithButtonsContainer>
                <CategoryContainer>
                    <TextInput disabled={!isEdit} value={nameValue} onChange={({ target }) => setName(target.value)} />
                    <PercentInput disabled={!isEdit} value={percentValue} onChange={({ target }) => setPercent(target.value)} type="number" min="1" max="100"/>
                    <InputStaticText style={{position: 'absolute', right: '75px', top: '39px'}}>%</InputStaticText>
                    {isEdit && (
                        <CategorySettings>
                            <SubText>когда</SubText>
                            <SelectStyled
                              labelId={key}
                              id={v4()}
                              value={typeValue}
                              onChange={({ target }) => setType(target.value)}
                              style={{width: '186px'}}
                            >
                              <MenuItem value={'total'}>сумма покупок</MenuItem>
                              <MenuItem value={'check'}>сумма чека</MenuItem>
                            </SelectStyled>
                            <SubText>больше</SubText>
                            <PercentInput value={limitValue} onChange={({ target }) => setLimit(target.value)} type="number" style={{marginTop: '18px', padding: '16px'}}/> <span style={{marginTop: '18px'}}>&#8381;</span>
                        </CategorySettings>
                    )}
                </CategoryContainer>
                {!isEdit && <RedactButton onClick={() => setEdit(true)}><img src={edit} /></RedactButton>}
                <RemoveButton onClick={() => onRemove(id)} isRedact={isEdit}><img src={remove} /></RemoveButton>
            </CategoryWithButtonsContainer>
            {isEdit && <SaveButton onClick={() => handleSave()}>Сохранить</SaveButton>}
        </>
    )
}