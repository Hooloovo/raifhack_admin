import styled from 'styled-components'
import Select from '@material-ui/core/Select';

export const CategoryWithButtonsContainer = styled.div`
    display: flex;
`

export const RedactButton = styled.button`
    cursor: pointer;
    width: 48px;
    height: 101px;
    background: #F8E94E;
    border-radius: 4px;
    margin-right: 4px;
    border: none;
    outline: none;
`

export const RemoveButton = styled.button`
    cursor: pointer;
    width: 48px;
    height: ${({ isRedact }) => isRedact ? '48px' : '101px'};
    background: #333333;
    border-radius: 4px;
    margin-right: 4px;
    border: none;
    outline: none;
`

export const CategoryContainer = styled.div`
    position: relative;
    width: 504px;
    padding: 24px;
    background: #F9F9FB;
    border-radius: 4px;
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 4px;
    margin-right: 4px;
`

export const TextInput = styled.input`
    width: 296px;
    height: 53px;
    margin-right: 24px;
    border: none;
    outline: none;
    padding: 16px;
    font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 21px;
color: #333333;
`

export const PercentInput = styled.input`
  width: 96px;
  height: 53px;
  background: #F9F9FB;
  border-radius: 4px;
  border: none;
  outline: none;
  padding: 16px 32px 16px 16px;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
  background: #FFFFFF;
`

export const InputStaticText = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
`

export const CategorySettings = styled.div`
    display: flex;
    align-items: center;
`

export const SubText = styled.span`
font-style: normal;
font-weight: normal;
font-size: 16px;
line-height: 18px;

/* Gray 3 */

color: #828282;
margin-right: 8px;
margin-top: 18px;
`

export const SelectStyled = styled(Select)`
    width: 186px;
    height: 50px;
    background-color: #fff;
    border-radius: 4px;
    margin-top: 18px;
    margin-right: 24px;
    padding: 16px;
`

export const SaveButton = styled.button`
    width: 504px;
    height: 48px;
    background: #333333;
    border-radius: 4px;
    border: none;
    outline: none;
    cursor: pointer;
    font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 21px;
/* identical to box height */


color: #FFFFFF;
margin-bottom: 40px;
`