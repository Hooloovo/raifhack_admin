import React from 'react'
import { Card, CardDescription, CardText, CardTitle, Container, SubmitButton } from './styled'
import arrow from './img/arrow.svg'

export const PersonalCards = ({ title, categoryName }) => {
    return (
        <Container>
            <Card>
                <CardTitle>{title}</CardTitle>
                <CardDescription>будет доступен до 13 ноября 2020</CardDescription>
                <CardText>Для всех клиентов с аккаунтом <b>{categoryName}</b></CardText>
            </Card>
            <SubmitButton>Отправить <img src={arrow} /></SubmitButton>
        </Container>
    )
}