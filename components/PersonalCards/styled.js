import styled from 'styled-components';

export const CardTitle = styled.span`
    font-style: normal;
font-weight: bold;
font-size: 24px;
line-height: 32px;
/* identical to box height */


color: #333333;
`

export const CardDescription = styled.span`
margin-bottom: 40px;
    font-style: normal;
font-weight: 500;
font-size: 13px;
line-height: 17px;

color: #333333;

opacity: 0.5;
`

export const CardText = styled.span`
    font-style: normal;
font-size: 16px;
line-height: 21px;

color: #333333;
`

export const Container = styled.div`
    width: 334px;
    display: flex;
    flex-direction: column;
`

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    padding: 50px 32px;
    background: #FFFFFF;
`

export const SubmitButton = styled.button`
    width: 100%;
    margin-top: 4px;
    background: #FFFFFF;
    padding: 20px 32px;
    display: flex;
    justify-content: space-between;
`