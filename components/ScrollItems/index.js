import React from 'react';
import { useRouter } from 'next/router';
import { ItemsContainer, ScrollContainer, SectionTitle } from "./styled";

export const ScrollItems = ({ children, title, primary }) => {
  const router = useRouter();

  return (
    <>
      <SectionTitle>{title}</SectionTitle>
      <ScrollContainer>
        <ItemsContainer isAnal={router.pathname === '/analytics'} itemsCount={React.Children.count(children)} primary={primary}>
          {children}
        </ItemsContainer>
      </ScrollContainer>
    </>
  );
}
