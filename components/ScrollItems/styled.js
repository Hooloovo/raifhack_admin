import styled from "styled-components";

export const SectionTitle = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin: 40px 48px;
`

export const ScrollContainer = styled.div`
  width: 100%;
  padding: 0 48px;
  overflow: auto;
  margin: 40px 0 60px 0;
  
  -ms-overflow-style: none;
  scrollbar-width: none;  
  &::-webkit-scrollbar {
    display: none;
  }
`

export const ItemsContainer = styled.div`
  width: ${({ itemsCount, isAnal }) => itemsCount * (isAnal ? 504 : 332)}px;
  padding: 60px 40px;
  background-color: ${({ primary }) => primary ? '#F8E94E' : '#F9F9FB'};
  display: flex;
  
  > div:not(:last-child) {
    margin-right: 40px;
  }
`
