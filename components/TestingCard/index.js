import moment from 'moment';
import React from 'react'
import { CardButton, CardDescription, CardLeftRight, CardTitle, CardTop, Container } from './styled'

export const TestingCard = ({ onSubmit, reason, categoryName, startDate, endDate, groups, id }) => {
    moment.locale('ru')
    let point;
    if (reason === 'IncreaseMidCheck') {
        point = 'Увеличение среднего чека'
    } else if (reason === 'ReturnClients') {
        point = 'Вернуть клиентов'
    } else [
        point = 'Увеличить посещаемость'
    ]
    return (
        <Container submitable={onSubmit !== undefined}>
            <CardTop>
                <CardTitle>{point}</CardTitle>
                <CardDescription><b>Аудитория: </b>клиенты категории {categoryName}, которые регулярно ходят к вам.</CardDescription>
                <CardDescription><b>Дата проведения:</b> с {moment(startDate, 'yy-MM-DD').format('DD MMMM YYYY')} по {moment(endDate, 'yy-MM-DD').format('DD MMMM YYYY')}</CardDescription>
            </CardTop>
            {groups.length === 1 ? (
                <CardLeftRight style={{ gridColumnStart: '1', gridColumnEnd: '3'}}>
                    <CardTitle>Скидка {groups[0].salePercent} %</CardTitle>
                    <CardDescription>Средний чек: </CardDescription>
                    <CardDescription>{groups[0].middleCheck} Р ({groups[0].checkIncreasePercent} %)</CardDescription>
                </CardLeftRight>
            ) : (
                <>
                    <CardLeftRight>
                        <CardTitle>Скидка {groups[0].salePercent} %</CardTitle>
                        <CardDescription>Средний чек: </CardDescription>
                        <CardDescription>{groups[0].middleCheck} Р ({groups[0].checkIncreasePercent} %)</CardDescription>
                    </CardLeftRight>
                    <CardLeftRight>
                        <CardTitle>Скидка {groups[1].salePercent} %</CardTitle>
                        <CardDescription>Средний чек: </CardDescription>
                        <CardDescription>{groups[1].middleCheck} Р ({groups[1].checkIncreasePercent} %)</CardDescription>
                    </CardLeftRight>
                </>
            )}
            {onSubmit !== undefined && <CardButton onClick={() => onSubmit(id)}>Завершить</CardButton>}
        </Container>
    )
}