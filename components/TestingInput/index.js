import React, { useState } from 'react'
import {
  Container,
  InputChange,
  InputChangeContainer,
  InputChangePercent,
  InputStatic,
  InputStaticText,
  RedactButton, RemoveButton
} from "./styled";
import redact from './img/redact.svg'
import remove from './img/remove.svg'

export const TestingInput = ({name, value, onChange, onRemove}) => {
  const [isRedact, setRedact] = useState(false);

  return (
    <Container>
      <InputStatic>
        <InputStaticText>{name}</InputStaticText>
        {isRedact ? (
          <>
            <InputChangePercent onChange={({ target }) => onChange(target.value)} type="number" min="1" max="100"/>
            <InputStaticText style={{position: 'absolute', right: '30px'}}>%</InputStaticText>
          </>
        ) : (
          <InputStaticText style={{position: 'absolute', right: '10px'}}>{value} %</InputStaticText>
        )}
      </InputStatic>
      {isRedact ? (
        <>
          <RedactButton style={{height: '85px'}} onClick={() => setRedact(false)}><img src={redact}/></RedactButton>
          <RemoveButton style={{height: '85px'}} onClick={() => onRemove()}><img src={remove}/></RemoveButton>
        </>
      ) : (
        <>
          <RedactButton onClick={() => setRedact(true)}><img src={redact}/></RedactButton>
          <RemoveButton onClick={() => onRemove()}><img src={remove}/></RemoveButton>
        </>
      )}
    </Container>
  )
}
