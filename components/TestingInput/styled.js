import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  width: 438px;
  position: relative;
  margin-bottom: 16px;
`

export const InputStatic = styled.div`
  width: 332px;
  min-height: 53px;
  background: #FFFFFF;
  border-radius: 4px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 16px;
  position: relative;
`

export const InputStaticText = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
`

export const InputChangeContainer = styled.div`
  width: 332px;
  height: 85px;
  position: relative;
  display: flex;
  align-items: center;
`

export const InputChange = styled.input`
  width: 100%;
  height: 100%;
  border: none;
  padding: 32px 16px;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
`

export const InputChangePercent = styled.input`
  // position: absolute;
  right: 16px;
  width: 96px;
  height: 53px;
  background: #F9F9FB;
  border-radius: 4px;
  border: none;
  outline: none;
  padding: 16px 32px 16px 16px;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
`

export const RedactButton = styled.button`
  width: 48px;
  height: 53px;
  background: #F8E94E;
  border-radius: 4px;
  border: none;
  cursor: pointer;
`

export const RemoveButton = styled.button`
  width: 48px;
  height: 53px;
  background: #333;
  border-radius: 4px;
  border: none;
  cursor: pointer;
`
