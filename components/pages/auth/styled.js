import styled from 'styled-components'
import Select from '@material-ui/core/Select';

export const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: #333333;
`

export const Title = styled.span`
    font-style: normal;
font-weight: bold;
font-size: 32px;
line-height: 43px;
/* identical to box height */


color: #FFFFFF;
margin-bottom: 48px;
`

export const ModalContainer = styled.div`
    width: 534px;
    height: 245px;
    display: flex;
    flex-direction: column;
    background: #F9F9FB;
    border-radius: 4px 4px 0 0;
`

export const TopModal = styled.div`
    width: 100%;
    height: 188px;
    padding: 32px 40px;
    display: flex;
    flex-direction: column;
`

export const ModalTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 24px;
    color: #333333;
    margin-bottom: 24px;
`

export const SelectStyled = styled(Select)`
    width: 454px;
    height: 65px;
    background-color: #fff;
    border-radius: 4px;
    margin-bottom: 43px;
    padding: 22px 32px;
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 21px;

    color: #333333;
`

export const EnterButton = styled.div`
    width: 534px;
    height: 57px;
    background: #F8E94E;
    border: none;
    outline: none;
    cursor: pointer;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 18px;
    text-align: center;
    color: #333333;
    display: flex;
    align-items: center;
    justify-content: center;
`