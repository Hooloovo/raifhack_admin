import styled from 'styled-components'

export const Container = styled.div`

    padding-top: 60px;
    width: 1346px;
    display: grid;
    grid-template-columns: 921px 344px;
    grid-column-gap: 81px;
    margin: auto;
`

export const MenuContainer = styled.div`
    width: 100%;
    height: 650px;
    padding: 40px;
    display: flex;
    flex-direction: column;
    background: #FFFFFF;
    border: 5px solid #333333;
    box-sizing: border-box;
`

export const SectionTable = styled.div`
    display: grid;
    grid-template-columns: 256px 256px 256px;
    grid-gap: 37px;
    grid-auto-rows: 48px;
    margin-bottom: 40px;
`

export const SectionTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 32px;
    color: #333333;
    margin-bottom: 24px;
`

export const ItemContainer = styled.div`
    width: 256px;
    height: 48px;
    display: flex;
    align-items: center;
`

export const ItemDesc = styled.div`
    width: 208px;
    height: 48px;
    background: #F2F2F2;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 16px;
`

export const ItemAddButton = styled.button`
    width: 48px;
    height: 48px;
    background: #333333;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`

export const ItemName = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 16px;
    color: #333333;
`

export const ItemPrice = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 14px;
    text-align: right;
    color: #333333;
`

export const CartContainer = styled.div`
    position: relative;
    width: 344px;
    height: 568px;
    background: #FFFFFF;
    border: 5px solid #F8E94E;
    box-sizing: border-box;
    padding: 32px 30px 0 30px;
    display: flex;
    flex-direction: column;
`

export const CartTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 32px;
    color: #333333;
    margin-bottom: 43px;
`

export const CartListContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const CartLine = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;
`

export const CartLineTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 21px;
    color: #333333;
`

export const CartPriceContainer = styled.div`
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 16px;
    /* identical to box height */

    text-align: right;

    /* Gray 1 */

    color: #333333;
`

export const PriceContainer = styled.div`
    position: absolute;
    top: 491px;
    left: -5px;
    width: 344px;
    height: 70px;
    padding: 23px 30px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    align-self: flex-end;
    border-top: 1px solid #333333;
`

export const PriceTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 24px;
    color: #333333;
`

export const BuyButton = styled.button`
    width: 344px;
    height: 80px;
    background: #F8E94E;
    background: #F8E94E;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 32px;
    cursor: pointer;
    /* identical to box height */


    /* Gray 1 */

    color: #333333;
`

export const QRContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const QRCode = styled.img`
    width: 446px;
    height: 446px;
`