import styled from 'styled-components'

export const Container = styled.div`
    display: grid;
    grid-template-columns: 604px 1fr;
    grid-column-gap: 203px;
    margin-bottom: 16px;
    padding: 0 48px;
`

export const SettingsContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`

export const Title = styled.span`
    font-style: normal;
font-weight: bold;
font-size: 24px;
line-height: 32px;
color: #333333;
`

export const CategoryContainer = styled.div`
    position: relative;
    width: 504px;
    padding: 24px;
    background: #F9F9FB;
    border-radius: 4px;
    display: flex;
    margin-bottom: 40px;
    margin-right: 4px;
`

export const TextInput = styled.input`
    width: 296px;
    height: 53px;
    margin-right: 24px;
    border: none;
    outline: none;
    padding: 16px;
    font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 21px;
color: #333333;
`

export const PercentInput = styled.input`
  width: 96px;
  height: 53px;
  background: #F9F9FB;
  border-radius: 4px;
  border: none;
  outline: none;
  padding: 16px 32px 16px 16px;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
  background: #FFFFFF;
`

export const InputStaticText = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
`

export const CategoryWithButtonsContainer = styled.div`
    display: flex;
`

export const RedactButton = styled.button`
    cursor: pointer;
    width: 48px;
    height: 101px;
    background: #F8E94E;
    border-radius: 4px;
    margin-right: 4px;
    border: none;
    outline: none;
`

export const RemoveButton = styled.button`
    cursor: pointer;
    width: 48px;
    height: ${({ isRedact }) => isRedact ? '48px' : '101px'};
    background: #333333;
    border-radius: 4px;
    margin-right: 4px;
    border: none;
    outline: none;
`

export const DescriptionContainer = styled.div`
    width: 100%;
    height: 731px;
    background: #F8E94E;
    border: 3px solid #F8E94E;
    box-sizing: border-box;
    border-radius: 4px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 35px 40px;
`

export const DescriptionBlock = styled.div`
    width: 100%;
    height: 308px;
    padding: 32px;
    display: flex;
    flex-direction: column;
    background-color: #fff;
`

export const DescriptionTitle = styled.span`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 24px;
    color: #333333;
    margin-bottom: 24px;
`

export const DescriptionText = styled.span`
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 21px;

    color: #333333;
`

export const AddCategory = styled.button`
    border: none;
    outline: none;
    background-color: transparent;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 18px;
    color: #828282;
    margin-top: 34px;
    cursor: pointer;
    align-self: flex-start;
    display: flex;
    align-items: center;
`