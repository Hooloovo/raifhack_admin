import styled from 'styled-components'

export const Container = styled.div`
  padding: 0 48px;
  display: grid;
  grid-template-columns: 518px 1fr;
  grid-column-gap: 172px;
`

export const SettingsContainer = styled.div`
  width: 100%;
  padding: 40px;
  background-color: #F9F9FB;
  display: flex;
  flex-direction: column;
  margin-bottom: 60px;
`

export const Title = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: #333333;
  margin-bottom: 24px;
`

export const FormTitle = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
  margin-bottom: 16px;
`

export const AddGroup = styled.button`
  display: flex;
  align-items: center;
  border: none;
  background: transparent;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 18px;
  color: #828282;
  cursor: pointer;
`

export const DateInput = styled.input`
  width: 186px;
  height: 50px;
  padding: 16px;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 18px;
  color: #333333;
  outline: none;
  border: none;
`

export const DescriptionContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const GroupCard = styled.div`
  width: 220px;
  height: 136px;
  display: flex;
  flex-direction: column;
  margin-right: 16px;
`

export const GroupCardTop = styled.div`
  width: 100%;
  height: 56px;
  background: #333333;
  padding: 16px;
  display: flex;
  justify-content: space-between;
  
  > span {
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 21px;
    color: #FFFFFF;
  }
`

export const GroupCardBottom = styled.div`
  width: 100%;
  height: 80px;
  padding: 16px;
  background: #F9F9FB;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  
  > span {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #333333;
  }
`

export const CardsContainer = styled.div`
  display: flex;
`

export const SubmitButton = styled.button`
  width: 456px;
  height: 56px;
  background: #F8E94E;
  display: flex;
  align-items: center;
  justify-content: center;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 21px;
  color: #333333;
  margin-top: 40px;
  cursor: pointer;
`