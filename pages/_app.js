import Header from '../components/Header'
import { useRouter } from 'next/router'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  return (
    <>
      {router.pathname !== '/auth' && router.pathname !== '/cart' && <Header />}
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
