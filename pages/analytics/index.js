import React, { useEffect, useState } from 'react';
import { disableTestingById, getTestingListById } from '../../api/testing';
import { ScrollItems } from '../../components/ScrollItems'
import { TestingCard } from '../../components/TestingCard';

const Analytics = () => {
  const [cards, setCards] = useState([])
  const [salonId, setSalonId] = useState();

  const onSubmit = (id) => {
    disableTestingById(id).then(() => getTestingListById(salonId).then(({ data }) => setCards(data.list)))
  }

  useEffect(() => {
    let id = window.localStorage.getItem('id');
    getTestingListById(id).then(({ data }) => setCards(data.list))
    setSalonId(id)
  }, [])

  return (
    <>
      
      {cards.filter(card => card.enabled).length !== 0 && (
        <ScrollItems primary title={'Тестирования в процессе'}>
          {cards.filter(card => card.enabled).map((card, i) => 
            <TestingCard { ...card } key={i} onSubmit={onSubmit}/>)}
        </ScrollItems>
      )}
      {cards.filter(card => !card.enabled).length !== 0 && (
        <ScrollItems title={'Завершенные тестирования'}>
          {cards.filter(card => !card.enabled).map((card, i) => 
              <TestingCard { ...card } key={i}/>)}
        </ScrollItems>
      )}
    </>
  );
}

export default Analytics;
