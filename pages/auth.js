import { useState } from 'react'
import { useRouter } from 'next/router'
import { MenuItem } from "@material-ui/core";
import { Container, ModalContainer, ModalTitle, Title, TopModal, SelectStyled, EnterButton } from "../components/pages/auth/styled";

export default function Home() {
    const [val, setVal] = useState(null)
    const router = useRouter();

    const onSubmit = () => {
        if (val !== null) {
            window.localStorage.setItem('id', val)
            router.push('/analytics');
        }
    }

    return (
      <Container>
          <Title>Авторизация</Title>
          <ModalContainer>
              <TopModal>
                  <ModalTitle>Выбери заведение</ModalTitle>
                  <SelectStyled value={val} onChange={({ target }) => setVal(target.value)}>
                      <MenuItem value='1'>Coffee Shop</MenuItem>
                      <MenuItem value='2'>Рататуй</MenuItem>
                      <MenuItem value='3'>Магазин одежды Кек</MenuItem>
                      <MenuItem value='4'>Coffee and Blues</MenuItem>
                      <MenuItem value='5'>Steak House</MenuItem>
                  </SelectStyled>
              </TopModal>
          </ModalContainer>
          <EnterButton onClick={() => onSubmit()}>Войти</EnterButton>
      </Container>
    )
  }
  