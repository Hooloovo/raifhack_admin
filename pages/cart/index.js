import React, { useState, useEffect } from 'react'
import { CartLineTitle, CartLine, CartContainer, ItemAddButton, ItemContainer, ItemName, MenuContainer, SectionTable, Container, SectionTitle, ItemDesc, ItemPrice, CartTitle, CartListContainer, CartPriceContainer, PriceContainer, PriceTitle, BuyButton, QRContainer, QRCode } from '../../components/pages/cart/styled'
import add from '../../components/pages/cart/img/plus.svg'
import remove from '../../components/pages/cart/img/remove.svg'
import { addToCart, createCartById, getListPositionsById, removeFromCart } from '../../api/shop'


const Cart = () => {
    const [salinId, setSalonId] = useState();
    const [cartId, setCartId] = useState();
    const [items, setItems] = useState([])
    const [cartItems, setCartItems] = useState([])
    const [cartSum, setCartSum] = useState()
    const [isPayed, setPayed] = useState(false)

    const addItem = (id) => {
        addToCart(cartId, { positionId: id, count: 1 }).then(({ data }) => {
            setCartItems(data.list);
            setCartSum(data.price);
        })
    }

    const removeItem = (id) => {
        removeFromCart(cartId, { positionId: id, count: 1 }).then(({ data }) => {
            setCartItems(data.list);
            setCartSum(data.price);
        })
    }

    useEffect(() => {
        let id = window.localStorage.getItem('id');
        getListPositionsById(id).then(({data}) => setItems(data.list))
        setSalonId(id)

        createCartById(id).then(({ data }) => setCartId(data.cartId))
    }, [])

    return (
        <Container>
            {isPayed ? (
                <>
                    <QRContainer>
                        <QRCode src={`https://api.qrserver.com/v1/create-qr-code/?size=500x500&data=exp://expo.io/@jack_shumeiko/RaifApp?cartId=${cartId}`}/>
                    </QRContainer>
                </>
            ) : (
                <>
                <MenuContainer style={isPayed ? {display: 'none'} : {}}>
                <SectionTitle>Кофе</SectionTitle>
                <SectionTable>
                    {items.filter(item => item.category === 'Кофе').map(item => <ItemContainer>
                        <ItemDesc>
                            <ItemName>{item.title}</ItemName>
                            <ItemPrice>{item.price} Р</ItemPrice>
                        </ItemDesc>
                        <ItemAddButton onClick={() => addItem(item.id)}><img src={add} /></ItemAddButton>
                    </ItemContainer>
                    )}
                </SectionTable>
                <SectionTitle>Выпечка</SectionTitle>
                <SectionTable>
                    {items.filter(item => item.category === 'Выпечка').map(item => <ItemContainer>
                        <ItemDesc>
                            <ItemName>{item.title}</ItemName>
                            <ItemPrice>{item.price} Р</ItemPrice>
                        </ItemDesc>
                        <ItemAddButton onClick={() => addItem(item.id)}><img src={add} /></ItemAddButton>
                    </ItemContainer>
                    )}
                </SectionTable>
                <SectionTitle>Прочее</SectionTitle>
                <SectionTable>
                    {items.filter(item => item.category === 'Прочее').map(item => <ItemContainer>
                        <ItemDesc>
                            <ItemName>{item.title}</ItemName>
                            <ItemPrice>{item.price} Р</ItemPrice>
                        </ItemDesc>
                        <ItemAddButton onClick={() => addItem(item.id)}><img src={add} /></ItemAddButton>
                    </ItemContainer>
                    )}
                </SectionTable>
            </MenuContainer>
                </>
            )}
            <div>
            <CartContainer>
                <CartTitle>Корзина</CartTitle>
                <CartListContainer>
                    {cartItems.map(item => <CartLine>
                        <CartLineTitle>{item.position.title} (x{item.count})</CartLineTitle>
                        <CartPriceContainer>{item.position.price} Р <img onClick={() => removeItem(item.position.id)} style={{marginLeft: '8px', cursor: 'pointer'}}src={remove} /></CartPriceContainer>
                    </CartLine>)}
                </CartListContainer>
                <PriceContainer>
                    <PriceTitle>Итого:</PriceTitle>
                    <PriceTitle>{cartSum} Р</PriceTitle>
                </PriceContainer>
            </CartContainer>
            <BuyButton onClick={() => cartSum > 0 && setPayed(true)}>Оплатить</BuyButton>
            </div>
        </Container>
    )
}

export default Cart