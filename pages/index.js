import { useRouter } from 'next/router';

export default function Home() {
  const router = useRouter();
  router.push('/auth')

  // const id = window.localStorage.getItem('id');

  // if (id === undefined) {
  //   router.push('/auth')
  // } else {
  //   router.push('/analytics')
  // }

  return (
    <>

    </>
  )
}

Home.getInitialProps = ({ res }) => {
  if (res) { // server
    res.writeHead(302, {
      Location: '/auth'
    });
  
    res.end();
  } else { // client
    Router.push('/auth');
  }
}
