import React, { useState, useEffect } from 'react'
import { Container, SettingsContainer, Title, DescriptionContainer, DescriptionBlock, DescriptionTitle, DescriptionText, AddCategory } from '../../components/pages/options/styled';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import { OptionsInput } from '../../components/OptionsInput';
import { getLoyaltiesById, setLoyaltiesById } from '../../api/shop';
import { v4 } from 'uuid'
import add from './img/add.svg'

const defaultCategory = {
    "categoryType": "OrderSum",
    "categoryName": "Basic",
    "categorySum": 1000,
    "categoryPercent": 5
}

const OptionsPage = () => {
    const [loyaltySystem, setSystem] = useState(1)
    const [optionsArray, setOptionsArray] = useState([])
    const [salonId, setSalonId] = useState()

    const onSave = (id, data) => {
        let newArray = optionsArray.slice();
        newArray[id] = data;
        setOptionsArray(newArray);

        setLoyaltiesById(salonId, {
            loyaltyType: +loyaltySystem === 1 ? 'Sale' : 'Cumulative',
            categories: newArray,
        })
    }

    const onRemove = (id) => {
        if (optionsArray.length > 1) {
            let newArray = optionsArray.slice();
            newArray.splice(id, 1);

            setOptionsArray([].concat(newArray));

            setLoyaltiesById(salonId, {
                loyaltyType: +loyaltySystem === 1 ? 'Sale' : 'Cumulative',
                categories: [].concat(newArray),
            })
        }
    }

    const onAdd = () => {
        if (optionsArray.length < 6) {
            setOptionsArray(optionsArray.concat(defaultCategory))
            setLoyaltiesById(salonId, {
                loyaltyType: +loyaltySystem === 1 ? 'Sale' : 'Cumulative',
                categories: optionsArray.concat(defaultCategory),
            })
        }
    }

    useEffect(() => {
        let id = window.localStorage.getItem('id')
        setSalonId(id)
        getLoyaltiesById(id).then(({ data }) => {
            setSystem(data.loyaltyType === 'Sale' ? 1 : 2)
            setOptionsArray(data.categories.map(elem => ({ ...elem.info})))
        })
    }, [])

    return (
        <Container>
            <SettingsContainer>
                <Title>Система лояльности</Title>
                <div style={{ marginBottom: '36px'}}>
                    <FormControlLabel checked={+loyaltySystem === 1} onChange={({ target }) => setSystem(target.value)} value="1" control={<Radio color="default"/>} label="Скидочная"/>
                    <FormControlLabel checked={+loyaltySystem === 2} onChange={({ target }) => setSystem(target.value)} value="2" control={<Radio color="default"/>} label="Бонусная"/>
                </div>
                <Title style={{marginBottom: '19px'}}>Категории</Title>
                {optionsArray.map((elem, i) => 
                <OptionsInput
                    key={v4()}
                    id={i}
                    name={elem.categoryName}
                    percent={elem.categoryPercent}
                    type={elem.categoryType === 'OrderSum' ? 'total' : 'check'}
                    limit={elem.categorySum}
                    onSave={onSave}
                    onRemove={onRemove}
                />)}
                <AddCategory onClick={() => onAdd()}><img src={add} style={{marginRight: '8px'}}/>Добавить категорию</AddCategory>
            </SettingsContainer>
            <DescriptionContainer>
                <DescriptionBlock>
                    <DescriptionTitle>Скидочная программа лояльности.</DescriptionTitle>
                    <DescriptionText>
                        Фиксированный процент скидки на каждую покупку. Выше категория клиента - больше скидка.
                    </DescriptionText>
                </DescriptionBlock>
                <DescriptionBlock>
                    <DescriptionTitle>
                        Бонусная система лояльности.
                    </DescriptionTitle>
                    <DescriptionText>
                        Чем больше покупаешь, тем больше начисляется баллов. Накопленные бонусы позволяют клиенту частично или полностью оплатить товар.
                    </DescriptionText>
                </DescriptionBlock>
            </DescriptionContainer>
        </Container>
    )
}

export default OptionsPage