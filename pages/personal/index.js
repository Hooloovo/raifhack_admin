import React, {useState, useEffect} from 'react';
import {ScrollItems} from "../../components/ScrollItems";
import { PersonalCards } from '../../components/PersonalCards';
import { suggestOffersById } from '../../api/shop';

const Analytics = () => {
  const [salonId, setSalonId] = useState();
  const [cards, setCards] = useState([])

  useEffect(() => {
    let id = window.localStorage.getItem('id')
    suggestOffersById(id).then(({ data }) => setCards(data.list))
  }, [])


  return (
    <>
      <ScrollItems title={'Проверенные акции'} primary>
        {cards.filter(card => card.tested).map(card => <PersonalCards title={card.offer.offerType === 'FreePosition' ? card.offer.freePosition.title : `Доп скидка ${card.offer.salePercent} %`} />)}
      </ScrollItems>
      <ScrollItems title={'Попробуйте новые акции'}>
        {cards.filter(card => !card.tested).map(card => <PersonalCards title={card.offer.offerType === 'FreePosition' ? card.offer.freePosition.title : `Доп скидка ${card.offer.salePercent} %`} />)}
      </ScrollItems>
    </>
  );
}

export default Analytics;
