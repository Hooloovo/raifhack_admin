import React, {useEffect, useState} from 'react';
import {
  AddGroup, CardsContainer,
  Container,
  DateInput,
  DescriptionContainer,
  FormTitle,
  GroupCard, GroupCardBottom, GroupCardTop,
  SettingsContainer,
  SubmitButton,
  Title
} from "../../components/pages/testing/styled";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import {TestingInput} from "../../components/TestingInput";
import plus from './img/plus.svg';
import moment from "moment";
import { getLoyaltiesById } from '../../api/shop';
import { setTestingDataById } from '../../api/testing';
import { useRouter } from 'next/router'

const Testing = () => {
  moment.locale('ru')
  const [point, setPoint] = useState('IncreaseMidCheck')
  const [category, setCategory] = useState(0)
  const [firstGroup, setFirst] = useState(3);
  const [secondGroup, setSecond] = useState(5);
  const [isOne, setOne] = useState(true);
  const [dateFrom, setFrom] = useState(moment().format('yy-MM-DD'));
  const [dateEnd, setEnd] = useState(moment().add(30, 'days').format('yy-MM-DD'));
  const [salonId, setSalonId] = useState();
  const [categories, setCategories] = useState([])
  const router = useRouter()

  const onSubmit = () => {
    if (categories.length > 0) {
      setTestingDataById(salonId, {
        reason: point,
        categoryId: categories[category].id,
        startDate: dateFrom,
        endDate: dateEnd,
        groups: isOne ? [
          {
            name: 'Тестовая группа 1',
            salePercent: firstGroup
          }
        ] : [
          {
            name: 'Тестовая группа 1',
            salePercent: firstGroup
          },
          {
            name: 'Тестовая группа 2',
            salePercent: secondGroup
          }
        ]
      }).then(() => router.push('/analytics'))
    }
  }

  useEffect(() => {
    const id = window.localStorage.getItem('id')
    setSalonId(id);
    getLoyaltiesById(id).then(({ data }) => {
      setCategories(data.categories)
    })
  }, [])

  return (
    <Container>
      <SettingsContainer>
        <Title>Настройка тестирования</Title>
        <FormControl component="fieldset">
          <FormTitle>Чего вы хотите добиться?</FormTitle>
          <RadioGroup aria-label="gender" name="gender" value={point} onChange={({target}) => setPoint(target.value)}>
            <FormControlLabel value="IncreaseMidCheck" control={<Radio color="default"/>} label="Увеличение среднего чека"/>
            <FormControlLabel value="ReturnClients" control={<Radio color="default"/>} label="Вернуть клиентов"/>
            <FormControlLabel value="IncreaseOrders" control={<Radio color="default"/>} label="Увеличить посещаемость"/>
          </RadioGroup>
        </FormControl>
        <FormControl component="fieldset">
          <FormTitle style={{marginTop: '40px'}}>На какую категорию?</FormTitle>
          <RadioGroup aria-label="gender" name="gender" value={category}
                      onChange={({target}) => setCategory(target.value)}>
              {categories.map((categoryElement, i) => <FormControlLabel
                key={i}
                value={i}
                checked={i === +category}
                control={<Radio color="default"/>}
                label={`${categoryElement.info.categoryName} (средний чек - ${categoryElement.stats.middleCheck} р)`}
                />)}
          </RadioGroup>
        </FormControl>
        <FormTitle style={{marginTop: '40px'}}>Размер дополнительной скидки:</FormTitle>
        {isOne ? (
          <>
            <TestingInput
              value={firstGroup}
              onChange={(val) => setFirst(val)}
              name={'Тестовая группа 1'}
              onRemove={() => setOne(true)}
            />
            <AddGroup onClick={() => setOne(false)}><img src={plus} style={{marginRight: '9px'}}/> Добавить тестовую
              группу</AddGroup>
          </>
        ) : (
          <>
            <TestingInput
              value={firstGroup}
              onChange={(val) => setFirst(val)}
              name={'Тестовая группа 1'}
              onRemove={() => setOne(true)}
            />
            <TestingInput
              value={secondGroup}
              onChange={(val) => setSecond(val)}
              name={'Тестовая группа 2'}
              onRemove={() => setOne(true)}
            />
          </>
        )}
        <FormTitle style={{marginTop: '40px'}}>Когда проводить тестирование?</FormTitle>
        <FormTitle style={{marginTop: '30px'}}>с <DateInput min={moment().format('yy-MM-DD')} type="date" value={dateFrom} onChange={({target}) => setFrom(target.value)}/> по <DateInput min={dateFrom} type="date" value={dateEnd} onChange={({target}) => setEnd(target.value)}/></FormTitle>
      </SettingsContainer>
      <DescriptionContainer>
        <Title style={{marginTop: '40px'}}>Тестирование</Title>
        <p>
          На сравнении с контрольной группой будет сделан вывод, на сколько успешно прошла акция.
        </p>
        <p>
          <b>Аудитория:</b> клиенты категории {categories.length > 0 && categories[category].info.categoryName}, которые регулярно ходят к вам.
        </p>
        <p>
          <b>Дата проведения:</b> с {moment(dateFrom, 'yy-MM-DD').format('DD MMMM YYYY')} по {moment(dateEnd, 'yy-MM-DD').format('DD MMMM YYYY')}
        </p>
        <Title style={{marginTop: '40px'}}>Текущие показатели</Title>
        <p>
          <b>Размер группы:</b> {categories.length > 0 && (isOne ? Math.floor(categories[category].stats.usersCount / 2) : Math.floor(categories[category].stats.usersCount / 4))} человек
        </p>
        <p>
          <b>Средний чек:</b> {categories.length > 0 && categories[category].stats.middleCheck} ₽
        </p>
        <p>
          <b>Текущая скидка:</b> {categories.length > 0 && categories[category].info.categoryPercent} %
        </p>
        <Title style={{marginTop: '40px'}}>Группы для тестирования</Title>
        <CardsContainer>
          <GroupCard>
            <GroupCardTop>
              <span>Тестовая 1</span>
              <span>{firstGroup} %</span>
            </GroupCardTop>
            <GroupCardBottom>
              <span>Количество людей в группе</span>
              <span>{categories.length > 0 && (isOne ? Math.floor(categories[category].stats.usersCount / 2) : Math.floor(categories[category].stats.usersCount / 4))}</span>
            </GroupCardBottom>
          </GroupCard>
          {!isOne && (
            <GroupCard>
              <GroupCardTop>
                <span>Тестовая 2</span>
                <span>{secondGroup} %</span>
              </GroupCardTop>
              <GroupCardBottom>
                <span>Количество людей в группе</span>
                <span>{categories.length > 0 && (isOne ? Math.floor(categories[category].stats.usersCount / 2) : Math.floor(categories[category].stats.usersCount / 4))}</span>
              </GroupCardBottom>
            </GroupCard>
          )}
        </CardsContainer>
        <SubmitButton onClick={() => onSubmit()}>Запустить тестирование</SubmitButton>
      </DescriptionContainer>
    </Container>
  );
}

export default Testing;
